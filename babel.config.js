module.exports = {
  presets: [
    '@babel/env',
    '@babel/react'
  ],
  ignore: [
    '**/node_modules/**',
    '**/*.test.js'
  ]
}
