import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'

import theme, { createTheme } from '../Theme'
console.log('Healthwise.ButtonGroup')

const StyledButtonGroup = styled.div.attrs({
  theme: props => createTheme({ ...props.theme, ...props.themeOverrides })
})`
  display: flex;
  align-content: center;
  justify-content: ${props => props.align === 'center'
    ? 'center'
    : align === 'right'
    ? 'flex-end'
    : 'flex-start'
  };
`

class ButtonGroup extends Component {
  render() {
    const { theme, children } = this.props
    return <StyledButtonGroup theme={theme} {...props}>{children}</StyledButtonGroup>
  }
}

ButtonGroup.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  align: PropTypes.oneOf(['left', 'center', 'right'])
}

ButtonGroup.defaultProps = {
  align: 'left',
  theme
}

export default withTheme(ButtonGroup)
