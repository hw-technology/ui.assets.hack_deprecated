console.log('Healthwise.Theme')

const theme = {
  colorPrimary: '#25a094',
  colorTextOnPrimary: '#fff',
  colorPrimaryLight: '#aae2d0',
  colorTextOnPrimaryLight: '#424242',
  focusIndicator: '2px dotted #424242',
  focusIndicatorContrast: '2px dotted #f4f4f4',
  focusIndicatorOffset: '2px',
  focusIndicatorInset: '-2px',
}

const createTheme = themeProps => ({
  ...theme,
  ...themeProps
})

const themeVariant = variable => props => {
  const { theme, themeVariant } = props
  switch (variable) {
    case 'colorText':
      switch (themeVariant) {
        case 'primaryLight':
          return theme.colorTextOnPrimaryLight
        case 'primary':
        default:
          return theme.colorTextOnPrimary
      }
    case 'focusIndicator':
      switch (themeVariant) {
        case 'primaryLight':
          return theme.focusIndicatorContrast
        case 'primary':
        default:
          return theme.focusIndicator
      }
    case 'color':
    default:
      switch (themeVariant) {
        case 'primaryLight':
          return theme.colorPrimaryLight
        case 'primary':
        default:
          return theme.colorPrimary
      }
  }
}

export { createTheme, themeVariant }
export default theme
