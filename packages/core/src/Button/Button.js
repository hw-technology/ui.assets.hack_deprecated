import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'

import theme, { createTheme, themeVariant } from '../Theme'
console.log('Healthwise.Button')

const StyledButton = styled.button.attrs({
  theme: props => {
    console.log('StyledButton theme', props.theme)
    return createTheme({ ...props.theme, ...props.themeOverrides })
  }
})`
  padding: 0.75rem 1rem;
  border: ${props => props.flat ? 'none' : `1px solid ${themeVariant('color')(props)}`};
  display: inline-flex;
  align-items: center;
  background-color: ${props => props.flat
    ? 'transparent'
    : themeVariant('color')(props)
  };
  color: ${props => props.flat
    ? themeVariant('color')(props)
    : themeVariant('colorText')(props)};
  font-size: 1em;
  font-style: normal;
  text-decoration: none;
  line-height: 1;
  cursor: pointer;

  &:focus {
    outline: ${themeVariant('focusIndicator')};
    outline-offset: ${props => props.theme.focusIndicatorOffset};
  }

  svg {
    fill: ${themeVariant('colorText')}
  }
`

StyledButton.defaultProps = {
  theme
}

class Button extends Component {
  render() {
    const {
      className,
      disabled,
      href,
      render,
      log,
      theme,
      ...otherProps
    } = this.props
    console.log(log)
    console.log('Button theme', theme)
    const ariaProps = href || otherProps.to ? { role: 'button', 'aria-disabled': disabled } : {}

    // Wrap all children in a div to allow setting margin on text nodes
    const children = React.Children.map(this.props.children, (child, index) => (
      <div key={index} className="child">
        {child}
      </div>
    ))

    const props = {
      className,
      disabled,
      href,
      ...ariaProps,
      ...otherProps,
    }

    if (render) {
      return render({ ...props, children })
    } else if (href) {
      return <a {...props}>{children}</a>
    } else {
      return <StyledButton theme={theme} {...props}>{children}</StyledButton>
    }
  }
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  render: PropTypes.func,
  disabled: PropTypes.bool,
  flat: PropTypes.bool,
  href: PropTypes.string,
  id: PropTypes.string,
  outlined: PropTypes.bool,
  raised: PropTypes.bool,
  rounded: PropTypes.bool,
  theme: PropTypes.object,
  themeOverrides: PropTypes.object,
  themeVariant: PropTypes.oneOf([
    'primary',
    'primaryLight',
    'primary-dark',
    'primary-darker',
    'accent',
    'accent-dark',
    'neutral',
    'neutral-light',
    'neutral-dark',
  ]),
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
}

Button.defaultProps = {
  themeVariant: 'primary'
}

export default withTheme(Button)
